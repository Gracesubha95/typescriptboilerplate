import * as React from 'react';
// const styles = require('../../../assets/styles.css');
// import * as classNameNames from 'classNamenames';



export namespace Products {

    export interface Props {
        productsname: string;
        productsid: string;

    }
    export interface State {
        productsname: string;
        productsid: string;
    }
}
export default class Products extends React.Component<Products.Props, Products.State> {
    constructor(props: Products.Props, context?: any) {
        super(props, context);
    }


    render() {

        return (

            <section className="content">
                <div className="row">
                    <div className="col-xs-12">
                        <div className="box box-primary">
                            <div className="box-header">
                                <div className="dropdown pull-right">
                                    <button className="btn btn-primary" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true"
                                        aria-expanded="false">
                                        SimplePOS (POS) <span className="caret"></span>
                                    </button>
                                    <ul className="dropdown-menu" aria-labelledby="dLabel">
                                    </ul>
                                </div>
                                <h3 className="box-title">Please use the table below to navigate or filter the results.</h3>
                            </div>
                            <div className="box-body">
                                <div className="table-responsive">
                                    <div id="prTables_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <div className="dataTables_length" id="prTables_length"><label>Show <select name="prTables_length"
                                                    aria-controls="prTables" className="form-control input-sm select2-hidden-accessible" tabIndex={1}
                                                    aria-hidden="true">
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                    <option value="-1">All</option>
                                                </select><span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: '75px' }}><span
                                                    className="selection"><span className="select2-selection select2-selection--single" role="combobox"
                                                        aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0}
                                                        aria-labelledby="select2-prTables_length-vc-container"><span className="select2-selection__rendered"
                                                            id="select2-prTables_length-vc-container" title="10">10</span><span className="select2-selection__arrow"
                                                                role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper"
                                                                    aria-hidden="true"></span></span> entries</label></div>
                                            </div>
                                            <div className="col-md-6 text-right pr0">
                                                <div className="dt-buttons btn-group"><a className="btn btn-default buttons-copy buttons-html5"
                                                    tabIndex={0} aria-controls="prTables" href="https://spos.tecdiary.com/products#"><span>Copy</span></a><a
                                                        className="btn btn-default buttons-excel buttons-html5" tabIndex={0} aria-controls="prTables"
                                                        href="https://spos.tecdiary.com/products#"><span>Excel</span></a><a className="btn btn-default buttons-csv buttons-html5"
                                                            tabIndex={0} aria-controls="prTables" href="https://spos.tecdiary.com/products#"><span>CSV</span></a><a
                                                                className="btn btn-default buttons-pdf buttons-html5" tabIndex={0} aria-controls="prTables"
                                                                href="https://spos.tecdiary.com/products#"><span>PDF</span></a><a className="btn btn-default buttons-collection buttons-colvis"
                                                                    tabIndex={0} aria-controls="prTables" href="https://spos.tecdiary.com/products#"><span>Columns</span></a></div>
                                            </div>
                                            <div id="prTables_processing" className="dataTables_processing panel panel-default" style={{ display: 'none' }}>Processing...</div>
                                        </div>
                                        <table id="prTables" className="table table-striped table-bordered table-hover dataTable" style={{ bottom: '5px', width: '1158px' }}
                                            role="grid" aria-describedby="prTables_info">
                                            <thead>
                                                <tr className="active" role="row">
                                                    <th style={{ width: '30px' }} className="sorting_disabled" rowSpan={1} colSpan={1} aria-label="Image">Image</th>
                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1}
                                                        aria-label="Code: activate to sort column ascending" style={{ width: '57px' }}>Code</th>
                                                    <th className="sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1} aria-label="Name: activate to sort column ascending"
                                                        style={{ width: '123px' }}>Name</th>
                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1}
                                                        aria-label="Type: activate to sort column ascending" style={{ width: '7px' }}>Type</th>
                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1}
                                                        aria-label="Category: activate to sort column ascending" style={{ width: '57px' }}>Category</th>
                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1}
                                                        aria-label="Quantity: activate to sort column ascending" style={{ width: '57px' }}>Quantity</th>
                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1}
                                                        aria-label="Tax: activate to sort column ascending" style={{ width: '57px' }}>Tax</th>
                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1}
                                                        aria-label="Method: activate to sort column ascending" style={{ width: '57px' }}>Method</th>
                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1}
                                                        aria-label="Cost: activate to sort column ascending" style={{ width: '57px' }}>Cost</th>
                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="prTables" rowSpan={1} colSpan={1}
                                                        aria-label="Price: activate to sort column ascending" style={{ width: '57px' }}>Price</th>
                                                    <th style={{ width: '165px' }} className="sorting_disabled" rowSpan={1} colSpan={1} aria-label="Actions">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr role="row" className="odd">
                                                    <td>
                                                        <div style={{ width: '32px', margin: '0 auto' }}><a href="https://spos.tecdiary.com/uploads/213c9e007090ca3fc93889817ada3115.png"
                                                            className="open-image"><img src="213c9e007090ca3fc93889817ada3115.webp" alt="" className="img-responsive" /></a></div>
                                                    </td>
                                                    <td>TOY02</td>
                                                    <td>Minion Banana</td>
                                                    <td>Standard</td>
                                                    <td>General</td>
                                                    <td>
                                                        <div className="text-center">
                                                            <div className="text-center">4.00</div>
                                                        </div>
                                                    </td>
                                                    <td>0</td>
                                                    <td><span className="label label-primary">Inclusive</span></td>
                                                    <td>
                                                        <div className="text-right">10.00</div>
                                                    </td>
                                                    <td>
                                                        <div className="text-right">15.00</div>
                                                    </td>
                                                    <td>
                                                        <div className="text-center">
                                                            <div className="btn-group"><a href="https://spos.tecdiary.com/products/view/2" title="View"
                                                                className="tip btn btn-primary btn-xs" data-toggle="ajax"><i className="fa fa-file-text-o"></i></a><a
                                                                    href="https://spos.tecdiary.com/products/single_barcode/2" title="Print Barcodes"
                                                                    className="tip btn btn-default btn-xs" data-toggle="ajax-modal"><i className="fa fa-print"></i></a>
                                                                <a href="https://spos.tecdiary.com/products/single_label/2" title="Print Labels" className="tip btn btn-default btn-xs"
                                                                    data-toggle="ajax-modal"><i className="fa fa-print"></i></a> <a className="tip image btn btn-primary btn-xs"
                                                                        id="Minion Banana (TOY02)" href="https://spos.tecdiary.com/uploads/213c9e007090ca3fc93889817ada3115.png"
                                                                        title="View Image"><i className="fa fa-picture-o"></i></a> <a href="https://spos.tecdiary.com/products/edit/2"
                                                                            title="Edit Product" className="tip btn btn-warning btn-xs"><i className="fa fa-edit"></i></a>
                                                                <a href="https://spos.tecdiary.com/products/delete/2"
                                                                    title="Delete Product" className="tip btn btn-danger btn-xs"><i className="fa fa-trash-o"></i></a></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr role="row" className="even">
                                                    <td>
                                                        <div style={{ width: '32px', margin: '0 auto' }}>
                                                            <a href="https://spos.tecdiary.com/uploads/6988655f95602f9394f9315165f920fe.png"
                                                                className="open-image">
                                                                <img src="6988655f95602f9394f9315165f920fe.webp" alt="" className="img-responsive" />
                                                            </a>
                                                        </div>
                                                    </td>
                                                    <td>TOY01</td>
                                                    <td>Minion Hi</td>
                                                    <td>Standard</td>
                                                    <td>General</td>
                                                    <td>
                                                        <div className="text-center">
                                                            <div className="text-center">-19,973.00</div>
                                                        </div>
                                                    </td>
                                                    <td>0</td>
                                                    <td><span className="label label-primary">Inclusive</span></td>
                                                    <td>
                                                        <div className="text-right">10.00</div>
                                                    </td>
                                                    <td>
                                                        <div className="text-right">15.00</div>
                                                    </td>
                                                    <td>
                                                        <div className="text-center">
                                                            <div className="btn-group"><a href="https://spos.tecdiary.com/products/view/1" title="View"
                                                                className="tip btn btn-primary btn-xs" data-toggle="ajax"><i className="fa fa-file-text-o"></i></a><a
                                                                    href="https://spos.tecdiary.com/products/single_barcode/1" title="Print Barcodes"
                                                                    className="tip btn btn-default btn-xs" data-toggle="ajax-modal"><i className="fa fa-print"></i></a>
                                                                <a href="https://spos.tecdiary.com/products/single_label/1" title="Print Labels" className="tip btn btn-default btn-xs"
                                                                    data-toggle="ajax-modal"><i className="fa fa-print"></i></a> <a className="tip image btn btn-primary btn-xs"
                                                                        id="Minion Hi (TOY01)" href="https://spos.tecdiary.com/uploads/6988655f95602f9394f9315165f920fe.png"
                                                                        title="View Image"><i className="fa fa-picture-o"></i></a> <a href="https://spos.tecdiary.com/products/edit/1"
                                                                            title="Edit Product" className="tip btn btn-warning btn-xs"><i className="fa fa-edit"></i></a>
                                                                <a href="https://spos.tecdiary.com/products/delete/1" className="tip btn btn-danger btn-xs"><i
                                                                    className="fa fa-trash-o"></i></a></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th style={{ width: '30px' }} rowSpan={1} colSpan={1}>Image</th>
                                                    <th className="col-xs-1" rowSpan={1} colSpan={1}>
                                                        <input type="text" className="text_filter" placeholder="[Code]" /></th>
                                                    <th rowSpan={1} colSpan={1}><input type="text" className="text_filter" placeholder="[Name]" /></th>
                                                    <th className="col-xs-1" rowSpan={1} colSpan={1}><input type="text" className="text_filter"
                                                        placeholder="[Type]" /></th>
                                                    <th className="col-xs-1" rowSpan={1} colSpan={1}><input type="text" className="text_filter"
                                                        placeholder="[Category]" /></th>
                                                    <th className="col-xs-1" rowSpan={1} colSpan={1}><input type="text" className="text_filter"
                                                        placeholder="[Quantity]" /></th>
                                                    <th className="col-xs-1" rowSpan={1} colSpan={1}><input type="text" className="text_filter"
                                                        placeholder="[Tax]" /></th>
                                                    <th className="col-xs-1" rowSpan={1} colSpan={1} >
                                                        <select className="select2 select_filter select2-hidden-accessible" tabIndex={1} aria-hidden="true">
                                                            <option value="">All</option>
                                                            <option value="0">Inclusive</option>
                                                            <option value="1">Exclusive</option>
                                                        </select>
                                                        <span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: '79px' }} />
                                                        <span className="selection">
                                                            <span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list"
                                                                aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-r02a-container">
                                                                <span className="select2-selection__rendered" id="select2-r02a-container" title="All">All</span>
                                                                <span className="select2-selection__arrow" role="presentation">
                                                                    <b role="presentation"></b>
                                                                </span>
                                                            </span>
                                                        </span>
                                                        <span className="dropdown-wrapper" aria-hidden="true">
                                                        </span>
                                                        
                                                        
                                                    
                                                     </th>
                                            <th className="col-xs-1" rowSpan={1} colSpan={1}>Cost</th>
                                            <th className="col-xs-1" rowSpan={1} colSpan={1}>Price</th>
                                            <th style={{ width: '165px' }} rowSpan={1} colSpan={1}>Actions</th>
                                     </tr>
                                        <tr>
                                            <td colSpan={11} className="p0" rowSpan={1}><input type="text" className="form-control b0" name="search_table"
                                                id="search_table" placeholder="Type &amp; hit enter to search the table" style={{ width: '100%' }} /></td>
                                        </tr>
                  </tfoot>
                </table>
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="dataTables_info" id="prTables_info" role="status" aria-live="polite">Showing 1 to 2
                      of 2 entries</div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="dataTables_paginate paging_simple_numbers" id="prTables_paginate">
                                            <ul className="pagination">
                                                <li className="paginate_button previous disabled" id="prTables_previous"><a href="https://spos.tecdiary.com/products#"
                                                    aria-controls="prTables" data-dt-idx="0" tabIndex={0}>Previous</a></li>
                                                <li className="paginate_button active"><a href="https://spos.tecdiary.com/products#"
                                                    aria-controls="prTables" data-dt-idx="1" tabIndex={0}>1</a></li>
                                                <li className="paginate_button next disabled" id="prTables_next"><a href="https://spos.tecdiary.com/products#"
                                                    aria-controls="prTables" data-dt-idx="2" tabIndex={0}>Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="clear"></div>
                            </div>
                        </div>
                        <div className="modal fade" id="picModal" tabIndex={1} role="dialog" aria-labelledby="picModalLabel"
                            aria-hidden="true">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <button type="button" className="close" data-dismiss="modal" aria-hidden="true"><i className="fa fa-times"></i></button>
                                        <button type="button" className="close mr10"><i className="fa fa-print"></i></button>
                                        <h4 className="modal-title" id="myModalLabel">title</h4>
                                    </div>
                                    <div className="modal-body text-center">
                                        <img id="product_image" src="https://spos.tecdiary.com/products" alt="" />
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
                </div>
                </div >
    

  </section >
  )

    }

}
