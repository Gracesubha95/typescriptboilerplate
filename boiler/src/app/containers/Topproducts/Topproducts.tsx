import * as React from 'react';
// const styles = require('../../../assets/styles.css');
// import * as classNameNames from 'classNamenames';



export namespace Topproducts {

    export interface Props {
        purchasename: string;
        purchaseid: string;

    }
    export interface State {
        purchasename: string;
        purchaseid: string;
    }
}
export default class Products extends React.Component<Topproducts.Props, Topproducts.State> {
    constructor(props: Topproducts.Props, context?: any) {
        super(props, context);
    }


    render() {

        return (
            <section className="content">
                <div className="row">
                    <div className="col-xs-12">
                        <div className="box box-primary">
                            <div className="box-header">
                                <h3 className="box-title">Please review the top products charts below</h3>
                            </div>
                            <div className="box-body">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="panel panel-default">
                                            <div className="panel-heading">This Month (December 2018)</div>
                                            <div className="panel-body">
                                                <div id="thisMonth" style={{ height: '400px' }} data-highcharts-chart="0"><div className="highcharts-container" id="highcharts-0" 
                                                style={{position: 'relative', overflow: 'hidden', width: '525px', height: '400px', textAlign: 'left', lineHeight: 'normal',  WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)'}}><svg version="1.1" style={{ fontFamily: '&quot,Lucida Grande&quot, &quot,Lucida Sans Unicode&quot, Arial, Helvetica, sansSerif', fontSize: '12px' }} xmlns="http://www.w3.org/2000/svg" width="525" height="400"><desc>Created with Highcharts 4.1.6</desc><defs>
                                                    <clipPath id="highcharts-1"><rect x="0" y="0" width="473" height="277"></rect>
                                                    </clipPath></defs><rect x="0" y="0" width="525" height="400" strokeWidth="0" fill="#FFFFFF" className=" highcharts-background"></rect><g className="highcharts-grid" z-index="1"></g><g className="highcharts-grid" z-index="1"><path fill="none" d="M 42 287.5 L 515 287.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path><path fill="none" d="M 42 232.5 L 515 232.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path><path fill="none" d="M 42 176.5 L 515 176.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path><path fill="none" d="M 42 121.5 L 515 121.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path><path fill="none" d="M 42 65.5 L 515 65.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path><path fill="none" d="M 42 9.5 L 515 9.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path></g><g className="highcharts-axis" z-index="2"><path fill="none" d="M 278.5 287 L 278.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 515.5 287 L 515.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 41.5 287 L 41.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 42 287.5 L 515 287.5" stroke="#C0D0E0" stroke-width="1" z-index="7" visibility="visible"></path></g><g className="highcharts-axis" z-index="2"></g><g className="highcharts-series-group" z-index="3"><g className="highcharts-series highcharts-tracker" visibility="visible" z-index="0.1" transform="translate(42,10) scale(1 1)" clip-path="url(#highcharts-1)"><rect x="60.5" y="55.5" width="114" height="222" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="297.5" y="166.5" width="114" height="111" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect></g><g className="highcharts-markers" visibility="visible" z-index="0.1" transform="translate(42,10) scale(1 1)" clip-path="none"></g></g><g className="highcharts-data-labels highcharts-tracker" visibility="visible" z-index="6" transform="translate(42,10) scale(1 1)" opacity="1" ><text x="121.5" y="30.5" r="0" z-index="1" transform="translate(0,0) rotate(-90 121.5 30.5)" style={{color: 'rgb(0, 0, 0)', fontSize: '12px', fontWeight: 'bold', fill: 'rgb(0, 0, 0)', textShadow: 'rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px', textRendering: 'geometricPrecision'}} text-anchor="end"><tspan>2</tspan></text>
                                                    <text x="358.5" y="141.5" r="0"  z-index="1" transform="translate(0,0) rotate(-90 358.5 141.5)" style={{ color: 'rgb(0, 0, 0)', fontSize: '12px', fontWeight: 'bold', fill: 'rgb(0, 0, 0)', textShadow: 'rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px', textRendering: 'geometricPrecision'}} text-anchor= "end">
                                                    <tspan>1</tspan>
                                                </text></g>
                                                    <g className="highcharts-axis-labels highcharts-xaxis-labels" z-index="7"><text x="164.00277674973256" style={{ color: '#606060', cursor: 'default', fontSize: '13px', fill: '#606060', width: '400px', textOverflow: 'ellipsis' }} textAnchor="end" transform="translate(0,0) rotate(-60 164.00277674973256 302)" y="302" opacity="1"><tspan>Minion Hi</tspan></text><text x="400.50277674973256" style={{color:'#606060',cursor:'default',fontSize:'13px',fill:'#606060',width:'400px',textOverflow:'ellipsis'}} text-anchor="end" transform="translate(0,0) rotate(-60 400.50277674973256 302)" y="302" opacity="1"><tspan>Minion Banana</tspan></text></g><g className="highcharts-axis-labels highcharts-yaxis-labels" z-index="7"><text x="27" style={{color:'#606060',cursor:'default',fontSize:'11px',fill:'#606060',width:'163px',textOverflow:'clip'}} text-anchor="end" transform="translate(0,0)" y="290" opacity="1">0</text><text x="27" style={{color:'#606060',cursor:'default',fontSize:'11px',fill:'#606060',width:'163px',textOverflow:'clip'}} text-anchor="end" transform="translate(0,0)" y="234" opacity="1">0.5</text><text x="27" style={{color:'#606060',cursor:'default',fontSize:'11px',fill:'#606060',width:'163px',textOverflow:'clip'}} text-anchor="end" transform="translate(0,0)" y="179" opacity="1">1</text><text x="27" style={{color:'#606060',cursor:'default',fontSize:'11px',fill:'#606060',width:'163px',textOverflow:'clip'}} text-anchor="end" transform="translate(0,0)" y="123" opacity="1">1.5</text><text x="27" style={{color:'#606060',cursor:'default',fontSize:'11px',fill:'#606060',width:'163px',textOverflow:'clip'}} text-anchor="end" transform="translate(0,0)" y="68" opacity="1">2</text><text x="27" style={{color:'#606060',cursor:'default',fontSize:'11px',fill:'#606060',width:'163px',textOverflow:'clip'}} text-anchor="end" transform="translate(0,0)" y="13" opacity="1">2.5</text></g><g className="highcharts-tooltip" z-index="8" style={{cursor:'default',padding:'0',whiteSpace:'nowrap'}} transform="translate(125,12)" opacity="1" visibility="visible"><path fill="none" d="M 3.5 0.5 L 67.5 0.5 C 70.5 0.5 70.5 0.5 70.5 3.5 L 70.5 44.5 C 70.5 47.5 70.5 47.5 67.5 47.5 L 40.5 47.5 34.5 53.5 28.5 47.5 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)" width="70" height="47"></path><path fill="none" d="M 3.5 0.5 L 67.5 0.5 C 70.5 0.5 70.5 0.5 70.5 3.5 L 70.5 44.5 C 70.5 47.5 70.5 47.5 67.5 47.5 L 40.5 47.5 34.5 53.5 28.5 47.5 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)" width="70" height="47"></path><path fill="none" d="M 3.5 0.5 L 67.5 0.5 C 70.5 0.5 70.5 0.5 70.5 3.5 L 70.5 44.5 C 70.5 47.5 70.5 47.5 67.5 47.5 L 40.5 47.5 34.5 53.5 28.5 47.5 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)" width="70" height="47"></path><path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 67.5 0.5 C 70.5 0.5 70.5 0.5 70.5 3.5 L 70.5 44.5 C 70.5 47.5 70.5 47.5 67.5 47.5 L 40.5 47.5 34.5 53.5 28.5 47.5 3.5 47.5 C 0.5 47.5 0.5 47.5 0.5 44.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" stroke="#7cb5ec" stroke-width="1"></path><text x="8" z-index="1" style={{
                                                        fontSize: '12px', color: '#333333', fill: '#333333'}} y='20'>
                                                        < tspan style={{fontSize: '10px'}}>Minion Hi
                                                </tspan>
                                                    <tspan style={{ fill: '#7cb5ec' }} x="8" dy="15">●</tspan>
                                                    <tspan dx="0"> Sold: </tspan><tspan style={{ fontWeight: 'bold' }} dx="0">2</tspan>
                                                </text>
                                                </g></svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="panel panel-default">
                                    <div className="panel-heading">Last Month (November 2018)</div>
                                    <div className="panel-body">
                                        <div id="lastMonth" style={{ height: '400px' }}></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="panel panel-default" style={{ marginBottom: 0 }}>
                                    <div className="panel-heading">Last 3 Months (From September 2018)</div>
                                    <div className="panel-body">
                                        <div id="lastQ" style={{ height: '400px' }} data-highcharts-chart="1">
                                            <div className="highcharts-container" id="highcharts-2" style={{ position: 'relative', overflow: 'hidden', width: '525px', height: '400px', textAlign: 'left', lineHeight: 'normal', zIndex: 0, WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)' }}>
                                                <svg version="1.1" style={{ fontFamily: '&quot,Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px' }} xmlns="http://www.w3.org/2000/svg" width="525" height="400"><desc>Created with Highcharts 4.1.6</desc><defs><clipPath id="highcharts-3"><rect x="0" y="0" width="473" height="277"></rect>
                                                </clipPath></defs><rect x="0" y="0" width="525" height="400" strokeWidth="0" fill="#FFFFFF" className=" highcharts-background"></rect><g className="highcharts-grid" z-index="1"></g>
                                                    <g className="highcharts-grid" z-index="1">
                                                        <path fill="none" d="M 42 287.5 L 515 287.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path>
                                                        <path fill="none" d="M 42 232.5 L 515 232.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path><path fill="none" d="M 42 176.5 L 515 176.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path><path fill="none" d="M 42 121.5 L 515 121.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path>
                                                        <path fill="none" d="M 42 65.5 L 515 65.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path>
                                                        <path fill="none" d="M 42 9.5 L 515 9.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1">
                                                        </path></g><g className="highcharts-axis" z-index="2"><path fill="none" d="M 278.5 287 L 278.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1">
                                                        </path>
                                                        <path fill="none" d="M 515.5 287 L 515.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                                                        <path fill="none" d="M 41.5 287 L 41.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                                                        <path fill="none" d="M 42 287.5 L 515 287.5" stroke="#C0D0E0" stroke-width="1" z-index="7" visibility="visible">
                                                        </path></g>
                                                    <g className="highcharts-axis" z-index="2"></g>
                                                    <g className="highcharts-series-group" z-index={3}><g className="highcharts-series highcharts-tracker" visibility="visible" z-index="0.1" transform="translate(42,10) scale(1 1)" clip-path="url(#highcharts-3)">
                                                        <rect x="60.5" y="55.5" width="114" height="222" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="297.5" y="166.5" width="114" height="111" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect>
                                                    </g>
                                                        <g className="highcharts-markers" visibility="visible" z-index="0.1" transform="translate(42,10) scale(1 1)" clip-path="none"></g>
                                                    </g>
                                                    <g className="highcharts-data-labels highcharts-tracker" visibility="visible" z-index="6" transform="translate(42,10) scale(1 1)" opacity="1" ><text x="121.5" y="30.5" r="0"  z-index="1" transform="translate(0,0) rotate(-90 121.5 30.5)" style={{ color: 'rgb(0, 0, 0)', fontSize: '12px', fontWeight: 'bold', fill: 'rgb(0, 0, 0)', textShadow: 'rgb(255, 255, 255) 0px 0px 6px rgb(255, 255, 255) 0px 0px 3px', textRendering: 'geometricPrecision' }} text-anchor="end"><tspan>2</tspan></text><text x="358.5" y="141.5" r="0"  z-index="1" transform="translate(0,0) rotate(-90 358.5 141.5)" style={{ color: 'rgb(0, 0, 0)', fontSize: '12px', fontWeight: 'bold', fill: 'rgb(0, 0, 0)', textShadow: 'rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px', textRendering: 'geometricPrecision' }} text-anchor="end">
                                                        <tspan>
                                                            1</tspan>
                                                    </text></g>
                                                    <g className="highcharts-axis-labels highcharts-xaxis-labels" z-index="7"><text x="164.00277674973256" style={{ color: '#606060', cursor: 'default', fontSize: '13px', fill: '#606060', width: '400px', textOverflow: 'ellipsis' }} text-anchor="end" transform="translate(0,0) rotate(-60 164.00277674973256 302)" y="302" opacity="1"><tspan>Minion Hi</tspan></text><text x="400.50277674973256" style={{ color: '#606060', cursor: 'default', fontSize: '13px', fill: '#606060', width: '400px', textOverflow: 'ellipsis' }} text-anchor="end" transform="translate(0,0) rotate(-60 400.50277674973256 302)" y="302" opacity="1">
                                                        <tspan>Minion Banana</tspan></text></g>
                                                    <g className="highcharts-axis-labels highcharts-yaxis-labels" z-index="7"><text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="290" opacity="1">0
                                                        </text><text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="234" opacity="1">0.5
                                                        </text><text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="179" opacity="1">1</text>
                                                        <text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="123" opacity="1">1.5
                                                        </text>
                                                        <text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="68" opacity="1">2</text>
                                                        <text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="13" opacity="1">2.5</text></g>
                                                    <g className="highcharts-tooltip" z-index="8" style={{ cursor: 'default', padding: '0', whiteSpace: 'nowrap' }} transform="translate(0,-9999)">
                                                        <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path>
                                                        <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path>
                                                        <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path>
                                                        <path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5"></path>
                                                        <text x="8" z-index="1" style={{ fontSize: '12px', color: '#333333', fill: '#333333' }} y="20"></text>
                                                    </g>
                                                </svg></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="panel panel-default" style={{ marginBottom: '0' }}>
                                    <div className="panel-heading">Last 12 Months (From December 2017)</div>
                                    <div className="panel-body">
                                        <div id="thisYear" style={{ height: '400px' }} data-highcharts-chart="2">
                                            <div className="highcharts-container" id="highcharts-4" style={{ position: 'relative', overflow: 'hidden', width: '525px', height: '400px', textAlign: 'left', lineHeight: 'normal', zIndex: 0, WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)' }}>
                                                <svg version="1.1" style={{ fontFamily: '&quot;Lucida Grande&quot;, &quot;Lucida Sans Unicode&quot;, Arial, Helvetica, sans-serif;font-size:12px' }} xmlns="http://www.w3.org/2000/svg" width="525" height="400">
                                                    <desc>Created with Highcharts 4.1.6</desc><defs>
                                                        <clipPath id="highcharts-5"><rect x="0" y="0" width="473" height="277"></rect>
                                                        </clipPath>
                                                    </defs><rect x="0" y="0" width="525" height="400" strokeWidth="0" fill="#FFFFFF" className=" highcharts-background"></rect>
                                                    <g className="highcharts-grid" z-index="1"></g>
                                                    <g className="highcharts-grid" z-index="1">
                                                        <path fill="none" d="M 42 287.5 L 515 287.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path>
                                                        <path fill="none" d="M 42 232.5 L 515 232.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path>
                                                        <path fill="none" d="M 42 176.5 L 515 176.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path>
                                                        <path fill="none" d="M 42 121.5 L 515 121.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path>
                                                        <path fill="none" d="M 42 65.5 L 515 65.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path><path fill="none" d="M 42 9.5 L 515 9.5" stroke="#D8D8D8" stroke-width="1" z-index="1" opacity="1"></path>
                                                    </g>
                                                    <g className="highcharts-axis" z-index="2"><path fill="none" d="M 278.5 287 L 278.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 515.5 287 L 515.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1"></path>
                                                        <path fill="none" d="M 41.5 287 L 41.5 297" stroke="#C0D0E0" stroke-width="1" opacity="1"></path><path fill="none" d="M 42 287.5 L 515 287.5" stroke="#C0D0E0" stroke-width="1" z-index="7" visibility="visible"></path></g>
                                                    <g className="highcharts-axis" z-index="2"></g><g className="highcharts-series-group" z-index="3"><g className="highcharts-series highcharts-tracker" visibility="visible" z-index="0.1" transform="translate(42,10) scale(1 1)" clip-path="url(#highcharts-5)"><rect x="60.5" y="55.5" width="114" height="222" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0"></rect><rect x="297.5" y="166.5" width="114" height="111" stroke="#FFFFFF" stroke-width="1" fill="#7cb5ec" rx="0" ry="0">
                                                    </rect></g>
                                                        <g className="highcharts-markers" visibility="visible" z-index="0.1" transform="translate(42,10) scale(1 1)" clip-path="none"></g></g><g className="highcharts-data-labels highcharts-tracker" visibility="visible" z-index="6" transform="translate(42,10) scale(1 1)" opacity="1" ><text x="121.5" y="30.5" r="0"  z-index="1" transform="translate(0,0) rotate(-90 121.5 30.5)" style={{ color: 'rgb(0, 0, 0)', fontSize: '12px', fontWeight: 'bold', fill: 'rgb(0, 0, 0)', textShadow: 'rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px', textRendering: 'geometricPrecision' }} text-anchor="end"><tspan>2</tspan></text>
                                                        <text x="358.5" y="141.5" r="0"  z-index="1" transform="translate(0,0) rotate(-90 358.5 141.5)" style={{ color: 'rgb(0, 0, 0)', fontSize: '12px', fontWeight: 'bold', fill: 'rgb(0, 0, 0)', textShadow: 'rgb(255, 255, 255) 0px 0px 6px, rgb(255, 255, 255) 0px 0px 3px', textRendering: 'geometricPrecision' }} text-anchor="end"><tspan>1</tspan>
                                                        </text></g>
                                                    <g className="highcharts-axis-labels highcharts-xaxis-labels" z-index="7"><text x="164.00277674973256" style={{ color: '#606060', cursor: 'default', fontSize: '13px', fill: '#606060', width: '400px', textOverflow: 'ellipsis' }} text-anchor="end" transform="translate(0,0) rotate(-60 164.00277674973256 302)" y="302" opacity="1">
                                                        <tspan>Minion Hi</tspan></text><text x="400.50277674973256" style={{ color: '#606060', cursor: 'default', fontSize: '13px', fill: '#606060', width: '400px', textOverflow: 'ellipsis' }} text-anchor="end" transform="translate(0,0) rotate(-60 400.50277674973256 302)" y="302" opacity="1">
                                                            <tspan>Minion Banana</tspan></text></g><g className="highcharts-axis-labels highcharts-yaxis-labels" z-index="7">
                                                        <text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="290" opacity="1">0</text>
                                                        <text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="234" opacity="1">0.5</text>
                                                        <text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="179" opacity="1">1
                                                    </text><text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060', width: '163px', textOverflow: 'clip' }} text-anchor="end" transform="translate(0,0)" y="123" opacity="1">1.5
                                                    </text><text x="27" style={{ color: '#606060', cursor: 'default', fontSize: '11px', fill: '#606060',width:'163px',textOverflow:'clip'}} text-anchor="end" transform="translate(0,0)" y="68" opacity="1">2</text>
                                                            < text x="27" style={{color: '#606060',cursor:'default',fontSize:'11px',fill:'#606060',width:'163px',textOverflow:'clip'}} text-anchor="end" transform="translate(0,0)" y="13" opacity="1">2.5
                                                    </text></g>
                                                <g className="highcharts-tooltip" z-index="8" style={{ cursor: 'default', padding: '0', whiteSpace: 'nowrap' }} transform="translate(0,-9999)">
                                                    <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.049999999999999996" stroke-width="5" transform="translate(1, 1)"></path>
                                                    <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.09999999999999999" stroke-width="3" transform="translate(1, 1)"></path>
                                                    <path fill="none" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5" is-shadow="true" stroke="black" stroke-opacity="0.15" stroke-width="1" transform="translate(1, 1)"></path>
                                                    <path fill="rgba(249, 249, 249, .85)" d="M 3.5 0.5 L 13.5 0.5 C 16.5 0.5 16.5 0.5 16.5 3.5 L 16.5 13.5 C 16.5 16.5 16.5 16.5 13.5 16.5 L 3.5 16.5 C 0.5 16.5 0.5 16.5 0.5 13.5 L 0.5 3.5 C 0.5 0.5 0.5 0.5 3.5 0.5"></path><text x="8" z-index="1" style={{ fontSize: '12px', color: '#333333', fill: '#333333' }} y="20">
                                                    </text></g>
                                                    </svg></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                                </div>
                                                </div >
                                                </div >
                                            </section >


        )

    }

}
