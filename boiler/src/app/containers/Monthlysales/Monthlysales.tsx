
import * as React from 'react';
// const styles = require('../../../assets/styles.css');
// import * as classNameNames from 'classNamenames';



export namespace Sales{

 export interface Props {
 salesname : string;
 salesid : string;
 
 }
 export interface State {
 salesname: string;
 salesid : string;
 }
}
export default class Sales extends React.Component<Sales.Props, Sales.State> {
constructor(props :Sales.Props,context?:any){
super(props,context);
}


render(){

 return(
<section className="content">
<div className="row">
<div className="col-xs-12">
<div className="box box-primary">
<div className="box-header">
<p>Please review the report below</p>
</div>
<div className="box-body">
<div className="col-sm-12">
<div className="row">
<div className="col-md-3 col-sm-6 col-xs-12">
<div className="info-box bg-aqua">
<span className="info-box-icon"><i className="fa fa-shopping-cart"></i></span>
<div className="info-box-content">
<span className="info-box-text">Sales Value</span>
<span className="info-box-number">47.25</span>
<div className="progress">
<div style={{width: '100%'}} className="progress-bar"></div>
</div>
<span className="progress-description">
1 Sales |
47.25 Received |
2.25 Tax </span>
</div>
</div>
</div>
<div className="col-md-3 col-sm-6 col-xs-12">
<div className="info-box bg-yellow">
<span className="info-box-icon"><i className="fa fa-plus"></i></span>
<div className="info-box-content">
<span className="info-box-text">Purchases Value</span>
<span className="info-box-number">0.00</span>
<div className="progress">
<div style={{width:"0%"}} className="progress-bar"></div>
</div>
<span className="progress-description">
0 Purchases </span>
</div>
</div>
</div>
<div className="col-md-3 col-sm-6 col-xs-12">
<div className="info-box bg-red">
<span className="info-box-icon"><i className="fa fa-circle-o"></i></span>
<div className="info-box-content">
<span className="info-box-text">Expenses Value</span>
<span className="info-box-number">0.00</span>
<div className="progress">
<div style={{width: '0%'}} className="progress-bar"></div>
</div>
<span className="progress-description">
0 Expenses </span>
</div>
</div>
</div>
<div className="col-md-3 col-sm-6 col-xs-12">
<div className="info-box bg-green">
<span className="info-box-icon"><i className="fa fa-dollar"></i></span>
<div className="info-box-content">
<span className="info-box-text">Profit and/or Loss</span>
<span className="info-box-number">47.25</span>
<div className="progress">
<div style={{width: "100%"}} className="progress-bar"></div>
</div>
<span className="progress-description">
47.2500 - 0 - 0 </span>
</div>
</div>
</div>

<div className="clearfix"></div>
<div className="calendar table-responsive">
<table className="table table-bordered" style={{width:"522px"}}>
<thead>
<tr className="active">
<th>
     <div className="text-center"> <a href="https://spos.tecdiary.com/reports/monthly_sales/2017">&lt;&lt;</a>
   </div>
</th>
<th colSpan={10}><div className="text-center"> 2018 
   </div>
</th>
<th>
    <div className="text-center"> <a href="https://spos.tecdiary.com/reports/monthly_sales/2019">&gt;&gt;</a>
     </div>
</th>

</tr>
</thead>
</table>
</div>
<tbody><tr>

<td>January</td>
<td>Febr/uary</td>
<td>March</td>
<td>April</td>
<td>May</td>
<td>June</td>
<td>July</td>
<td>August</td>
<td>September</td>
<td>October</td>
<td>November</td>
<td>December</td>
</tr>
</tbody>


<tr>
<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
<table className="table table-condensed table-striped table-calendar" style={{marginBottom:'0'}}>
<tbody>
    <tr>
        <td style={{textAlign:'left',fontWeight:'bold'}}>Total<br/>
<span style={{float:'right',fontWeight:'bold'}}>45.00</span> 
</td>
</tr>
</tbody>
</table>



<tr>
    <td style={{textAlign:'left',fontWeight:'bold'}}>
<span style={{fontWeight:'normal'}}>Product Tax</span><br/>
<span style={{float:'right'}}>0.00</span><br/>Order Tax<br/>
<span style={{float:'right'}}>2.25</span><br/>
<span style={{fontWeight:'bold'}}>Tax</span><br/>
<span style={{float:'right',fontWeight:'bold'}}>2.25</span>
</td>
</tr>

<tr>
    <td style={{textAlign:'left',fontWeight:'bold'}} className="violet">Discount<br/>
<span style={{float:'right',fontWeight:'bold'}}>0.00</span>
</td>
</tr>

<tr>
    <td style={{textAlign:'left',fontWeight:'bold'}} className="violet">Grand Total<br/>
<span style={{float:'right',fontWeight:'bold'}} className="violet">47.25</span>
</td>
</tr>

<tr>
    <td style={{textAlign:'left',fontWeight:'bold'}} className="green">Paid<br/>
<span style={{float:'right',fontWeight:'bold'}} className="green">47.25</span>
</td>
</tr>

<tr>
    <td style={{textAlign:'left',fontWeight:'bold'}} className="orange">Balance<br/>
<span style={{float:'right',fontWeight:'bold'}} className="orange">0.00</span>
</td></tr>



</div>
</div>
</div>
</div>
</div>
</div>


</section>
    
 )

}

}

	
	
	
