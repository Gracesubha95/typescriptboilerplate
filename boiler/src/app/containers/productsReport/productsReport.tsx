import * as React from 'react';
// const styles = require('../../../assets/styles.css');
// import * as classNames from 'classnames';



export namespace ProductsReport {

    export interface Props {
        productsreportname: string;
        productsreportid: string;

    }
    export interface State {
        productsreportname: string;
        productsreportid: string;
    }
}
export default class ProductsReport extends React.Component<ProductsReport.Props, ProductsReport.State> {
    constructor(props: ProductsReport.Props, context?: any) {
        super(props, context);
    }


    render() {


        return (

            <section className="content">
                <div className="row">
                    <div className="col-xs-12">
                        <div className="box box-primary">
                            <div className="box-header">
                                <a href="https://spos.tecdiary.com/reports/products#" className="btn btn-default btn-sm toggle_form pull-right">Show/Hide Form</a>
                                <h3 className="box-title">You can customize the report as your requirement</h3>
                            </div>
                            <div className="box-body">
                                <div id="form" className="panel panel-warning" style={{ display: 'none' }}>
                                    <div className="panel-body">
                                        <form action="https://spos.tecdiary.com/reports/products" method="post" accept-charset="utf-8">
                                            <input type="hidden" name="spos_token" value="ea2adc5b61aa200018dac5fbae4ad2b7" />
                                            <div className="row">
                                                <div className="col-xs-4">
                                                    <div className="form-group">
                                                        <label className="control-label" >Product</label>
                                                        <select name="product" className="form-control select2 select2-hidden-accessible" style={{ width: '100%' }} id="product" tabIndex={-1} aria-hidden="true">
                                                            <option value="0">Select Product</option>
                                                            <option value="1">Minion Hi</option>
                                                            <option value="2">Minion Banana</option>
                                                        </select>
                                                        <span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: '100%' }}>
                                                            <span className="selection">
                                                                <span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-product-container">
                                                                    <span className="select2-selection__rendered" id="select2-product-container" title="Select Product">Select Product</span>
                                                                    <span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>
                                                                </span>
                                                            </span>
                                                            <span className="dropdown-wrapper" aria-hidden="true"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div className="col-xs-4">
                                                    <div className="form-group">
                                                        <label className="control-label" >Start Date</label>
                                                        <input type="text" name="start_date" value="" className="form-control datetimepicker" id="start_date"/>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-4">
                                                        <div className="form-group">
                                                            <label className="control-label">End Date</label>
                                                            <input type="text" name="end_date" value="" className="form-control datetimepicker" id="end_date"/>
                                                        </div>
                                                        </div>
                                                        <div className="col-xs-12">
                                                            <button type="submit" className="btn btn-primary">Submit</button>
                                                        </div>
                                                    </div>
                                            </form>
                                            </div>
                                            </div>
                                        <div className="clearfix">
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-12">
                                                <div className="table-responsive">
                                                    <div id="PrRData_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
                                                        <div className="row">
                                                            <div className="col-md-6"><div className="dataTables_length" id="PrRData_length"><label>Show
    <select name="PrRData_length" aria-controls="PrRData" className="form-control input-sm select2-hidden-accessible" tabIndex={-1} aria-hidden="true">
                                                                    <option value="10">10</option>
                                                                    <option value="25">25</option>
                                                                    <option value="50">50</option>
                                                                    <option value="100">100</option>
                                                                    <option value="-1">All</option>
                                                                </select>
                                                                <span className="select2 select2-container select2-container--default" dir="ltr" style={{ width: '75px' }}>
                                                                    <span className="selection">
                                                                        <span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-PrRData_length-l1-container">
                                                                            <span className="select2-selection__rendered" id="select2-PrRData_length-l1-container" title="10">10</span>
                                                                            <span className="select2-selection__arrow" role="presentation"><b role="presentation"></b>
                                                                            </span>
                                                                        </span>
                                                                    </span>
                                                                    <span className="dropdown-wrapper" aria-hidden="true">
                                                                    </span>
                                                                </span> entries</label>
                                                            </div>
                                                            </div>
                                                            <div className="col-md-6 text-right pr0">
                                                                <div className="dt-buttons btn-group">
                                                                    <a className="btn btn-default buttons-copy buttons-html5" tabIndex={0} aria-controls="PrRData" href="https://spos.tecdiary.com/reports/products#">
                                                                        <span>Copy
        </span>
                                                                    </a>
                                                                    <a className="btn btn-default buttons-excel buttons-html5" tabIndex={0} aria-controls="PrRData" href="https://spos.tecdiary.com/reports/products#">
                                                                        <span>Excel
            </span>
                                                                    </a>
                                                                    <a className="btn btn-default buttons-csv buttons-html5" tabIndex={0} aria-controls="PrRData" href="https://spos.tecdiary.com/reports/products#">
                                                                        <span>CSV
                </span>
                                                                    </a>
                                                                    <a className="btn btn-default buttons-pdf buttons-html5" tabIndex={0} aria-controls="PrRData" href="https://spos.tecdiary.com/reports/products#">
                                                                        <span>PDF</span></a>
                                                                    <a className="btn btn-default buttons-collection buttons-colvis" tabIndex={0} aria-controls="PrRData" href="https://spos.tecdiary.com/reports/products#">
                                                                        <span>Columns</span></a>
                                                                </div>
                                                            </div>
                                                            <div id="PrRData_processing" className="dataTables_processing panel panel-default" style={{ display: 'none' }}>Processing...
                </div>
                                                        </div>
                                                        <table id="PrRData" className="table table-striped table-bordered table-hover dataTable" style={{ marginBottom: '5px', width: '1158px' }} role="grid" aria-describedby="PrRData_info">
                                                            <thead>
                                                                <tr className="active" role="row"><th className="col-xs-2 sorting" tabIndex={0} aria-controls="PrRData" rowSpan={1} colSpan={1} aria-label="Code: activate to sort column ascending" style={{ width: '154px' }}>Code
            </th>
                                                                    <th className="sorting" tabIndex={0} aria-controls="PrRData" rowSpan={1} colSpan={1} aria-label="Name: activate to sort column ascending" style={{ width: '446px' }}>Name
            </th>
                                                                    <th className="col-xs-1 sorting" tabIndex={0} aria-controls="PrRData" rowSpan={1} colSpan={1} aria-label="Sold: activate to sort column ascending" style={{ width: '57px' }}>Sold</th><th className="col-xs-1 sorting" tabIndex={0} aria-controls="PrRData" rowSpan={1} colSpan={1} aria-label="Tax: activate to sort column ascending" style={{ width: '57px' }}>Tax</th><th className="col-xs-1 sorting" tabIndex={0} aria-controls="PrRData" rowSpan={1} colSpan={1} aria-label="Cost: activate to sort column ascending" style={{ width: '57px' }}>Cost</th><th className="col-xs-1 sorting" tabIndex={0} aria-controls="PrRData" rowSpan={1} colSpan={1} aria-label="Income: activate to sort column ascending" style={{ width: '57px' }}>Income</th><th className="col-xs-1 sorting" tabIndex={0} aria-controls="PrRData" rowSpan={1} colSpan={1} aria-label="Profit: activate to sort column ascending" style={{ width: '58px' }}>Profit
            </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                <tr role="row" className="odd">
                                                                    <td>TOY02</td>
                                                                    <td>Minion Banana</td>
                                                                    <td>
                                                                        <div className="text-center">
                                                                            <div className="text-center">2.00</div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-right">0.00</div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-right">20.00</div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-right">30.00</div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-right">10.00</div>
                                                                    </td>
                                                                </tr>
                                                                <tr role="row" className="even">
                                                                    <td>TOY01</td>
                                                                    <td>Minion Hi</td>
                                                                    <td>
                                                                        <div className="text-center">
                                                                            <div className="text-center">3.00
                                </div>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-right">0.00</div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-right">30.00</div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-right">45.00</div>
                                                                    </td>
                                                                    <td>
                                                                        <div className="text-right">15.00</div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                <tr className="active">
                                                                    <th className="col-sm-2" rowSpan={1} colSpan={1}>
                                                                        <input type="text" className="text_filter" placeholder="[Code]"/>
                             </th>
                                                                        <th rowSpan={1} colSpan={1}>
                                                                            <input type="text" className="text_filter" placeholder="[Name]"/>
                             </th>
                                                                            <th className="col-xs-1" rowSpan={1} colSpan={1}>5
                             </th>
                                                                            <th className="col-xs-1" rowSpan={1} colSpan={1}><div className="text-right">0.00</div>
                                                                            </th>
                                                                            <th className="col-xs-1" rowSpan={1} colSpan={1}>
                                                                                <div className="text-right">50.00</div>
                                                                            </th>
                                                                            <th className="col-xs-1" rowSpan={1} colSpan={1}>
                                                                                <div className="text-right">75.00</div>
                                                                            </th>
                                                                            <th className="col-xs-1" rowSpan={1} colSpan={1}>
                                                                                <div className="text-right">25.00</div>
                                                                            </th>
                             </tr>
                                                                        <tr>
                                                                            <td colSpan={7} className="p0" rowSpan={1}>
                                                                                <input type="text" className="form-control b0" name="search_table" id="search_table" placeholder="Type &amp; hit enter to search the table" style={{ width: '100%' }}/>
                            </td>
                            </tr>
                    </tfoot>
                </table>
                                                                    <div className="row"><div className="col-md-6"><div className="dataTables_info" id="PrRData_info" role="status" aria-live="polite">Showing 1 to 2 of 2 entries</div></div><div className="col-md-6"><div className="dataTables_paginate paging_simple_numbers" id="PrRData_paginate"><ul className="pagination"><li className="paginate_button previous disabled" id="PrRData_previous"><a href="https://spos.tecdiary.com/reports/products#" aria-controls="PrRData" data-dt-idx="0" tabIndex={0}>Previous</a></li><li className="paginate_button active"><a href="https://spos.tecdiary.com/reports/products#" aria-controls="PrRData" data-dt-idx="1" tabIndex={0}>1</a></li><li className="paginate_button next disabled" id="PrRData_next"><a href="https://spos.tecdiary.com/reports/products#" aria-controls="PrRData" data-dt-idx="2" tabIndex={0}>Next</a></li></ul></div></div></div><div className="clear"></div></div>
                        </div>
                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        </section>

                                )
                              
                              }
                              
                              }
                              
