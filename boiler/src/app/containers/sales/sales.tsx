


import * as React from 'react';
// const styles = require('../../../assets/styles.css');
// import * as classNames from 'classnames';



export namespace Sales{

 export interface Props {
 salesname : string;
 salesid : string;
 
 }
 export interface State {
 salesname: string;
 salesid : string;
 }
}
export default class Sales extends React.Component<Sales.Props, Sales.State> {
constructor(props :Sales.Props,context?:any){
super(props,context);
}


render(){

 return(
     
<section className="content">
<div className="row">
<div className="col-xs-12">
<div className="box box-primary">
<div className="box-header">
<h3 className="box-title">Please use the table below to navigate or filter the results.</h3>
</div>
</div>
<div className="box-body">
<div className="table-responsive">
<div id="SLData_wrapper" className="dataTables_wrapper form-inline dt-bootstrap">
<div className="row">
<div className="col-md-6">
<div className="dataTables_length" id="SLData_length">
<label>Show <select name="SLData_length" aria-controls="SLData" className="form-control input-sm select2-hidden-accessible" tabIndex={-1} aria-hidden="true">
<option value="10">10</option>
<option value="25">25</option>
<option value="50">50</option>
<option value="100">100</option>
<option value="-1">All</option>
</select>
<span className="select2 select2-container select2-container--default" dir="ltr" style={{width: '75px'}}>
<span className="selection">
<span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-SLData_length-v1-container">
<span className="select2-selection__rendered" id="select2-SLData_length-v1-container" title="10">10</span>
<span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span>
<span className="dropdown-wrapper" aria-hidden="true"></span></span> entries</label></div></div>
<div className="col-md-6 text-right pr0">
<div className="dt-buttons btn-group">
<a className="btn btn-default buttons-copy buttons-html5" tabIndex={0} aria-controls="SLData" href="https://spos.tecdiary.com/sales#"><span>Copy</span></a>
<a className="btn btn-default buttons-excel buttons-html5" tabIndex={0} aria-controls="SLData" href="https://spos.tecdiary.com/sales#"><span>Excel</span></a>
<a className="btn btn-default buttons-csv buttons-html5" tabIndex={0} aria-controls="SLData" href="https://spos.tecdiary.com/sales#"><span>CSV</span></a>
<a className="btn btn-default buttons-pdf buttons-html5" tabIndex={0} aria-controls="SLData" href="https://spos.tecdiary.com/sales#"><span>PDF</span></a>
<a className="btn btn-default buttons-collection buttons-colvis" tabIndex={0} aria-controls="SLData" href="https://spos.tecdiary.com/sales#"><span>Columns</span></a>
</div></div><div id="SLData_processing" className="dataTables_processing panel panel-default" style={{display: 'none'}}>Processing...</div></div>
<table id="SLData" className="table table-striped table-bordered table-condensed table-hover dataTable" role="grid" aria-describedby="SLData_info" style={{width: '1158px'}}>
<thead>
<tr className="active" role="row">
<th className="col-xs-2 sorting" tabIndex={0} aria-controls="SLData" rowSpan={1} colSpan={1} aria-label="Date: activate to sort column ascending" style={{width: '152px'}}>Date</th>
<th className="sorting" tabIndex={0} aria-controls="SLData" rowSpan={1} colSpan={1} aria-label="Customer: activate to sort column ascending" style={{width: '156px'}}>Customer</th>
<th className="col-xs-1 sorting" tabIndex={0} aria-controls="SLData" rowSpan={1} colSpan={1} aria-label="Total: activate to sort column ascending" style={{width: '69px'}}>Total</th>
<th className="col-xs-1 sorting" tabIndex={0} aria-controls="SLData" rowSpan={1} colSpan={1} aria-label="Tax: activate to sort column ascending" style={{width: '68px'}}>Tax</th>
<th className="col-xs-1 sorting" tabIndex={0} aria-controls="SLData" rowSpan={1} colSpan={1} aria-label="Discount: activate to sort column ascending" style={{width: '69px'}}>Discount</th>
<th className="col-xs-1 sorting" tabIndex={0} aria-controls="SLData" rowSpan={1} colSpan={1} aria-label="Grand Total: activate to sort column ascending" style={{width: '157px'}}>Grand Total</th>
<th className="col-xs-1 sorting" tabIndex={0} aria-controls="SLData" rowSpan={1} colSpan={1} aria-label="Paid: activate to sort column ascending" style={{width: '69px'}}>Paid</th>
<th className="col-xs-1 sorting" tabIndex={0} aria-controls="SLData" rowSpan={1} colSpan={1} aria-label="Status: activate to sort column ascending" style={{width: '70px'}}>Status</th>
<th  className="sorting_disabled" rowSpan={1} colSpan={1} aria-label="Actions" style={{ width: '115px'}}>Actions</th></tr>
</thead>
<tbody>

<tr role="row" className="odd" id="3">
<td>Sun 2 Dec 2018 07:41 PM</td>
<td>Walk-in Client</td>
<td><div className="text-right">30.00</div></td>
<td><div className="text-right">1.50</div></td>
<td><div className="text-right">0.00</div></td>
<td><div className="text-right">31.50</div></td>
<td><div className="text-right">31.50</div></td>
<td><div className="text-center"><span className="sale_status label label-success">Paid</span></div></td>
<td><div className="text-center"><div className="btn-group"><a href="https://spos.tecdiary.com/pos/view/3/1" title="View sale" className="tip btn btn-primary btn-xs" data-toggle="ajax-modal"><i className="fa fa-list"></i></a> 
<a href="https://spos.tecdiary.com/sales/payments/3" title="View Payments" className="tip btn btn-primary btn-xs" data-toggle="ajax"><i className="fa fa-money"></i></a> 
<a href="https://spos.tecdiary.com/sales/add_payment/3" title="Add Payment" className="tip btn btn-primary btn-xs" data-toggle="ajax"><i className="fa fa-briefcase"></i></a> 
<a href="https://spos.tecdiary.com/pos/?edit=3" title="Edit Sale" className="tip btn btn-warning btn-xs"><i className="fa fa-edit"></i></a> 
<a href="https://spos.tecdiary.com/sales/delete/3"  title="Delete Sale" className="tip btn btn-danger btn-xs"><i className="fa fa-trash-o"></i></a>
</div></div></td></tr>
<tr role="row" className="even" id="2">
<td>Sun 2 Dec 2018 07:03 PM</td>
<td>Walk-in Client</td>
<td><div className="text-right">300,225.00</div></td>
<td><div className="text-right">15,011.25</div></td>
<td><div className="text-right">0.00</div></td>
<td><div className="text-right">315,236.25</div></td>
<td><div className="text-right">315,236.25</div></td>
<td><div className="text-center"><span className="sale_status label label-success">Paid</span></div></td>
<td><div className="text-center"><div className="btn-group"><a href="https://spos.tecdiary.com/pos/view/2/1" title="View sale" className="tip btn btn-primary btn-xs" data-toggle="ajax-modal"><i className="fa fa-list"></i></a> 
<a href="https://spos.tecdiary.com/sales/payments/2" title="View Payments" className="tip btn btn-primary btn-xs" data-toggle="ajax"><i className="fa fa-money"></i></a> 
<a href="https://spos.tecdiary.com/sales/add_payment/2" title="Add Payment" className="tip btn btn-primary btn-xs" data-toggle="ajax"><i className="fa fa-briefcase"></i></a> 
<a href="https://spos.tecdiary.com/pos/?edit=2" title="Edit Sale" className="tip btn btn-warning btn-xs"><i className="fa fa-edit"></i></a> 
<a href="https://spos.tecdiary.com/sales/delete/2"  title="Delete Sale" className="tip btn btn-danger btn-xs"><i className="fa fa-trash-o"></i></a></div></div></td></tr>
<tr role="row" className="odd" id="1">
<td>Mon 23 May 2016 01:28 PM</td><td>Walk-in Client</td>
<td><div className="text-right">30.00</div></td>
<td><div className="text-right">1.50</div></td>
<td><div className="text-right">0.00</div></td><td>
    <div className="text-right">31.50</div></td>
    <td><div className="text-right">31.50</div></td>
    <td><div className="text-center"><span className="sale_status label label-success">Paid</span></div></td>
    <td><div className="text-center"><div className="btn-group">
    <a href="https://spos.tecdiary.com/pos/view/1/1" title="View sale" className="tip btn btn-primary btn-xs" data-toggle="ajax-modal"><i className="fa fa-list"></i></a> 
    <a href="https://spos.tecdiary.com/sales/payments/1" title="View Payments" className="tip btn btn-primary btn-xs" data-toggle="ajax"><i className="fa fa-money"></i></a> 
    <a href="https://spos.tecdiary.com/sales/add_payment/1" title="Add Payment" className="tip btn btn-primary btn-xs" data-toggle="ajax"><i className="fa fa-briefcase"></i></a> 
    <a href="https://spos.tecdiary.com/pos/?edit=1" title="Edit Sale" className="tip btn btn-warning btn-xs"><i className="fa fa-edit"></i></a> 
    <a href="https://spos.tecdiary.com/sales/delete/1"  title="Delete Sale" className="tip btn btn-danger btn-xs"><i className="fa fa-trash-o"></i></a>
    </div></div></td></tr></tbody>
<tfoot>
<tr className="active">
<th className="col-sm-2" rowSpan={1} colSpan={1}>
<span className="datepickercon">
<input type="text" className="text_filter datepicker" placeholder="[Date]" />
</span>
</th>
<th className="col-sm-2" rowSpan={1} colSpan={1}>
<input type="text" className="text_filter" placeholder="[Customer]"/></th>
<th className="col-sm-1" rowSpan={1} colSpan={1}><div className="text-right">300,285.00</div></th>
<th className="col-sm-1" rowSpan={1} colSpan={1}><div className="text-right">15,014.25</div></th>
<th className="col-sm-1" rowSpan={1} colSpan={1}><div className="text-right">0.00</div></th>
<th className="col-sm-2" rowSpan={1} colSpan={1}><div className="text-right">315,299.25</div></th>
<th className="col-sm-1" rowSpan={1} colSpan={1}><div className="text-right">315,299.25</div></th>
<th className="col-sm-1" rowSpan={1} colSpan={1}>
<select className="select2 select_filter select2-hidden-accessible" tabIndex={-1} aria-hidden="true">
<option value="">All</option>
<option value="paid">Paid</option>
<option value="partial">Partial</option>
<option value="due">Due</option>
</select>
<span className="select2 select2-container select2-container--default" dir="ltr" style={{width: '83px'}}>
<span className="selection">
<span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-84va-container">
<span className="select2-selection__rendered" id="select2-84va-container" title="All">All</span>
<span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span>
<span className="dropdown-wrapper" aria-hidden="true"></span></span>
</th>
<th className="col-sm-1" rowSpan={1} colSpan={1}>Actions</th></tr>
<tr>
    <td  className="p0"  rowSpan={1} colSpan={1}>
    <input type="text" className="form-control b0" name="search_table" id="search_table" placeholder="Type &amp; hit enter to search the table" style={{width:'100%'}} />
    </td>
    </tr>
</tfoot>
</table>
<div className="row">
<div className="col-md-6">
<div className="dataTables_info" id="SLData_info" role="status" aria-live="polite">Showing 1 to 3 of 3 entries</div></div>
<div className="col-md-6">
<div className="dataTables_paginate paging_simple_numbers" id="SLData_paginate">
<ul className="pagination">
<li className="paginate_button previous disabled" id="SLData_previous">
<a href="https://spos.tecdiary.com/sales#" aria-controls="SLData" data-dt-idx="0" tabIndex={0}>Previous</a></li>
<li className="paginate_button active">
<a href="https://spos.tecdiary.com/sales#" aria-controls="SLData" data-dt-idx="1" tabIndex={0}>1</a></li>
<li className="paginate_button next disabled" id="SLData_next">
<a href="https://spos.tecdiary.com/sales#" aria-controls="SLData" data-dt-idx="2" tabIndex={0}>Next</a></li></ul>
</div></div></div>
<div className="clear"></div></div>
</div>
<div className="clearfix"></div>
</div>
</div>
</div>

</section>
 )

}

}

