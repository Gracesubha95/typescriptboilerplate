
import * as React from 'react';
// const styles = require('../../../assets/styles.css');
// import * as classNames from 'classnames';



export namespace AddProduct{

    export interface Props {
    productname : string;
    productid   : string;
    
    }
    export interface State {
        productname:  string;
        productid  :  string;
      }
}
export default class AddProduct extends React.Component<AddProduct.Props, AddProduct.State> {
constructor(props :AddProduct.Props,context?:any){
super(props,context);
}


render(){

    return(
<section className="content">
   <div className="row">
      <div className="col-xs-12">
         <div className="box box-primary">
            <div className="box-header">
               <h3 className="box-title">Please fill in the information below</h3>
            </div>
            <div className="box-body">
               <div className="col-lg-12">
                  <form  >
                     <button type="submit" className="fv-hidden-submit" style={{display: 'none', width: '0px', height: '0px'}}></button>
                     <input type="hidden" name="spos_token" value="ea2adc5b61aa200018dac5fbae4ad2b7"/>
                     <div className="row">
                        <div className="col-md-6">
                           <div className="form-group">
                              <label>Type</label> 
                              <select name="type" className="form-control tip select2 select2-hidden-accessible" id="type"   style={{width:'100%'}} data-fv-field="type" tabIndex={-1} aria-hidden="true">
                                 <option value="standard" >Standard</option>
                                 <option value="combo">Combo</option>
                                 <option value="service">Service</option>
                              </select>
                              <span className="select2 select2-container select2-container--default" dir="ltr" style={{width: '100%'}}><span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-type-container"><span className="select2-selection__rendered" id="select2-type-container" title="Standard">Standard</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="type" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group">
                              <label>Name</label> <input type="text" name="name" value="" className="form-control tip" id="name"   data-fv-field="name"/>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="name" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group">
                              <label>Code</label> you can use product barcode as code <input type="text" name="code" value="" className="form-control tip" id="code"   data-fv-field="code"/>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="code" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group all">
                              <label>Barcode Symbology</label> 
                              <select name="barcode_symbology" className="form-control select2 select2-hidden-accessible" id="barcode_symbology"   style={{width:'100%'}} data-fv-field="barcode_symbology" tabIndex={-1} aria-hidden="true">
                                 <option value="code25">Code25</option>
                                 <option value="code39">Code39</option>
                                 <option value="code128">Code128</option>
                                 <option value="ean8">EAN8</option>
                                 <option value="ean13">EAN13</option>
                                 <option value="upca ">UPC-A</option>
                                 <option value="upce">UPC-E</option>
                              </select>
                              <span className="select2 select2-container select2-container--default" dir="ltr" style={{width: '100%'}}><span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-barcode_symbology-container"><span className="select2-selection__rendered" id="select2-barcode_symbology-container" title="Code128">Code128</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="barcode_symbology" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group">
                              <label>Category</label> 
                              <select name="category" className="form-control select2 tip select2-hidden-accessible" id="category"   style={{width:'100%'}} data-fv-field="category" tabIndex={-1} aria-hidden="true">
                                 <option value="">Select Category</option>
                                 <option value="1">General</option>
                              </select>
                              <span className="select2 select2-container select2-container--default" dir="ltr" style={{width: '100%'}}><span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-category-container"><span className="select2-selection__rendered" id="select2-category-container" title="Select Category">Select Category</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="category" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group st">
                              <label>Cost</label> <input type="text" name="cost" value="" className="form-control tip" id="cost"/>
                           </div>
                           <div className="form-group">
                              <label>Price</label> <input type="text" name="price" value="" className="form-control tip" id="price"   data-fv-field="price"/>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="price" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group">
                              <label>Product Tax</label> Percentage without % sign i.e 4 for 4 percent <input type="text" name="product_tax" value="0" className="form-control tip" id="product_tax"   data-fv-field="product_tax"/>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="product_tax" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group">
                              <label>Tax Method</label> 
                              <select name="tax_method" className="form-control tip select2 select2-hidden-accessible" id="tax_method"   style={{width:'100%'}} data-fv-field="tax_method" tabIndex={-1} aria-hidden="true">
                                 <option value="0">Inclusive</option>
                                 <option value="1">Exclusive</option>
                              </select>
                              <span className="select2 select2-container select2-container--default" dir="ltr" style={{width: '100%'}}><span className="selection"><span className="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabIndex={0} aria-labelledby="select2-tax_method-container"><span className="select2-selection__rendered" id="select2-tax_method-container" title="Inclusive">Inclusive</span><span className="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span className="dropdown-wrapper" aria-hidden="true"></span></span>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="tax_method" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group st">
                              <label>Alert Quantity</label> <input type="text" name="alert_quantity" value="0" className="form-control tip" id="alert_quantity"   data-fv-field="alert_quantity"/>
                              <small className="help-block" data-fv-validator="notEmpty" data-fv-for="alert_quantity" data-fv-result="NOT_VALIDATED" style={{display: 'none'}}>Please enter a value</small>
                           </div>
                           <div className="form-group">
                              <label>Image</label> <input type="file" name="userfile" id="image" tabIndex={-1} style={{position: 'absolute', clip: 'rect(0px, 0px, 0px, 0px)'}}/>
                              <div className="bootstrap-filestyle input-group"><input type="text" className="form-control " /> <span className="group-span-filestyle input-group-btn" tabIndex={0}><label className="btn btn-default "><span className="fa fa-folder-open"></span> Choose file</label></span></div>
                           </div>
                        </div>
                        <div className="col-md-6">
                           <div id="ct" style={{display:'none'}}>
                              <div className="form-group">
                                 <label>Add Products</label> <input type="text" name="add_item" value="" className="form-control ttip ui-autocomplete-input" id="add_item" data-placement="top" data-trigger="focus" data-bv-notempty-message="Please add items below" placeholder="Add Item"/>
                              </div>
                              <div className="control-group table-group">
                                 <label className="table-label">Combo Products</label>
                                 <div className="controls table-controls">
                                    <table id="prTable" className="table items table-striped table-bordered table-condensed table-hover">
                                       <thead>
                                          <tr>
                                             <th className="col-xs-9">Product Name (Product Code)</th>
                                             <th className="col-xs-2">Quantity</th>
                                             <th className=" col-xs-1 text-center"><i className="fa fa-trash-o trash-opacity-50"></i></th>
                                          </tr>
                                       </thead>
                                       <tbody></tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                           <div className="">
                              <div className="well well-sm">
                                 <h4>SimplePOS (POS)</h4>
                                 <div className="form-group st">
                                    <label>Quantity</label> <input type="text" name="quantity1" value="0" className="form-control tip" id="quantity1"/>
                                 </div>
                                 <div className="form-group" style={{marginBottom:'0'}}>
                                    <label>Price</label> <input type="text" name="price1" value="" className="form-control tip" id="price1" placeholder="Optional"/>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div className="form-group">
                        <label>Details</label> 
                        <div className="redactor-box">
                           <ul className="redactor-toolbar" id="redactor-toolbar-0" style={{position: 'relative', width: 'auto', top: '0px', left: '0px', visibility: 'visible'}}>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-html" rel="html" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-formatting" rel="formatting" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-bold" rel="bold" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-italic" rel="italic" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-deleted" rel="deleted" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-unorderedlist" rel="unorderedlist" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-orderedlist" rel="orderedlist" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-outdent" rel="outdent" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-indent" rel="indent" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-link" rel="link" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-alignment" rel="alignment" tabIndex={-1}></a></li>
                              <li><a href="https://spos.tecdiary.com/products/add#" className="re-icon re-horizontalrule" rel="horizontalrule" tabIndex={-1}></a></li>
                           </ul>
                           <div className="redactor-editor redactor-linebreaks"  dir="ltr" style={{minHeight: '150px', maxHeight: '500px'}}></div>
                           <textarea name="details" cols={40} rows={10} className="form-control tip redactor" id="details" dir="ltr" style={{display: 'none'}}></textarea>
                        </div>
                     </div>
                     <div className="form-group">
                        <input type="submit" name="add_product" value="Add Products" className="btn btn-primary"/>
                     </div>
                  </form>
               </div>
               <div className="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
</section>
       
    )

}

}