import * as React from 'react';
import { Route, Switch } from 'react-router';
import { App as TodoApp } from 'app/containers/App';
import { hot } from 'react-hot-loader';
import Addproduct from '../app/containers/addproduct/addproduct'
import Topproducts from '../app/containers/Topproducts/Topproducts'
import products from '../app/containers/products/products'
import Sales from '../app/containers/sales/sales'
import ProductsReport from './containers/productsReport/productsReport'
import Monthlysales from './containers/Monthlysales/Monthlysales'
export const App = hot(module)(() => (
  <Switch>
    <Route exact path="/" component={TodoApp} />
    <Route path="/addproduct" component={Addproduct} />
    <Route path="/Monthlysales" component={Monthlysales} />
    <Route path="/Topproducts" component={Topproducts} />
    <Route path="/sales" component={Sales} />
    <Route path="/products" component={products} />
    <Route path="/productsReport" component={ProductsReport} />
  </Switch>
));
